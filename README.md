# Cloud Services Comparison
![CloudServicesComparison01](images/CloudServicesComparison01.jpg)

source: https://d2m6ke2px6quvq.cloudfront.net/uploads/2020/08/10/80e3f9ce-60d7-40f1-8234-f3f852138274.jpg

![CloudServicesComparison02](images/CloudServicesComparison02.png)

source: https://mildaintrainings.com/wp-content/uploads/2019/09/Compute-Services.png

![CloudServicesComparison03](images/CloudServicesComparison03.jpg)

source: https://1.bp.blogspot.com/-crYM-k-k5L4/Xq-7ofRGIoI/AAAAAAAAFy4/z0dcQyEvCEUspPgkyJ1UCHXsmas9xFQdQCLcBGAsYHQ/s1600/Azure_VS_AWS_VS_Google%2BCompute.jpg

![CloudServicesComparison04](images/CloudServicesComparison04.png)

source: https://i.pinimg.com/originals/a0/29/3c/a0293c8d9b387a207e7da434723c9a92.png

# For Developers
| Service | AWS | Azure |
|---------|-----|-------|
Ticket tracking | | Azure Board
CI/CD | CodePipeline | Azure Pipelines
Git server | CodeCommit | Azure Repos
 | | Azure Test Plans
Package/Binary store | CodeArtifact | Azure Artifacts
Web IDE | Cloud9 | Visual Studio Code

# For a list of services/products per cloud provider:
* AWS: https://aws.amazon.com/products/
* AZURE: https://azure.microsoft.com/en-ca/services/
* Google Cloud: https://cloud.google.com/products
# ==================================================
# 02h02m_PLURALSIGHT_Microsoft Azure - The Big Picture by Matt Milner 2019
Q: Explain the different cloud offering?  
A:

![IaasVsPaasVsSaas](images/IaasVsPaasVsSaas.png)

Q: What are the main category of Azure services?  
A:

![MicrosoftCloudAzureServiceCategory](images/MicrosoftCloudAzureServiceCategory.png)

![MicrosoftAzureBuildingBlock](images/MicrosoftAzureBuildingBlock.png)

Q: What are the main services of Azure compute?  
A:

![AzureComputeServices](images/AzureComputeServices.png)

1. VM
2. Azure App Service = Hosting for web app
3. Containers:
    * Azure Kubernetes Service (AKS)
    * Azure Container Instances
    * Azure Container Registry
4. Serverless:
    * Azure Functions
    * Logic Apps
    * Event Grid
5. Compute at scale:
    * Azure Batch
    * HD Insight

Q: What are the main services of Azure storage?  
A:
1. Relational databases:
    * Azure SQL
    * MySQL
    * MariaDB
    * Postgresql
2. Other:
   * Table Storage
   * Blob Storage
   * Queues Storage
   * Redis Cache
3. Services-based:
   * Azure CosmosDB
   * Azure DataLakes

Q: What are the main services for Messaging and Events?  
A:
1. Azure Service Bus
2. Event Grid
3. API Management
4. Logic Apps

Q: What are the main services for networking in Azure?  
A:
1. Virtual Network:
2. Express Route
3. CDN
4. Traffic Manager
5. Load Balancer
6. DNS Zones
7. DDOS Protection
8. Application Gateway
9. Front Door

Q: What are the main services for monitoring Azure?  
A:
# ==================================================
# AZ-900_09h51m_PEARSON_Exam AZ-900 - Microsoft Azure Fundamentals (Video), 2nd Edition 2021
Q: What is the cloud pyramid?  
A:

![TheCloudPyramid](images/TheCloudPyramid.png)

![OnPremiseVsCloud](images/OnPremiseVsCloud.jpg)

![AzureIaasVsPaasVsSaas](images/AzureIaasVsPaasVsSaas.png)
source: https://azure.microsoft.com/en-ca/overview/what-is-iaas/

Q: Examples of Iaas?  
A:
* Azure VM
* Azure Security Center

Q: Examples of Paas?  
A:
* Azure App Services

Q: Examples of Saas?  
A:
* Office 365

Q: Examples of serverless?  
A:
We are borrowing a computer/VM for a small period of time.
* Azure Functions
* Azure Logic Apps
* Azure Cognitive Service

Q: Geography vs Region vs Availability zone?  
A:
source: https://azure.microsoft.com/en-ca/global-infrastructure/geographies/

Q: What is a "Resource Group"?  
A:

![AzureResourceGroup](images/AzureResourceGroup.png)

Q: What types of subscription exist for Azure?  
A:
1. Free Trial
2. Pay-as-you-go
3. Pay-as-yu-go-dev/test
4. Developer Support

Q: What is Azure Management Group?  
A:

![AzureManagementGroup](images/AzureManagementGroup.png)

Q: What is Azure Resource Manager?  
A:

![AzureResourceManager](images/AzureResourceManager.png)

Q: What is Azure Availability Set?  
A:

![AzureAvailabilitySet](images/AzureAvailabilitySet.png)

Q: Why use a virtual network?  
A:

![AzureVNetAndSubnet](images/AzureVNetAndSubnet.png)

Q: What is Azure VPN Gateway?  
A:

![AzureVPNGateway](images/AzureVPNGateway.png)
Types:
1. VNet to VNet
2. Site to Site
3. Point to Site

Q: VPN Gateway vs VNet peering?  
A:

Which is best for you?
While we offer two ways to connect VNets, based on your specific scenario and needs, you might want to pick one over the other.
* VNet Peering provides a low latency, high bandwidth connection useful in scenarios such as cross-region data replication and database failover scenarios. 
  Since traffic is completely private and remains on the Microsoft backbone, customers with strict data policies prefer to use VNet Peering as public internet is not involved.
  Since there is no gateway in the path, there are no extra hops, ensuring low latency connections.
* VPN Gateways provide a limited bandwidth connection and is useful in scenarios where encryption is needed, but bandwidth restrictions are tolerable. 
In these scenarios, customers are also not as latency-sensitive.
  
source: https://azure.microsoft.com/en-us/blog/vnet-peering-and-vpn-gateways/

![AzureVNetPeering01](images/AzureVNetPeering01.png)

![AzureVNetPeering02](images/AzureVNetPeering02.png)

Q: Why use ExpressRoute?  
A:

![AzureExpressRoute](images/AzureExpressRoute.png)

Q: How do you persist VM data even after deletion?  
A:

![AzureDiskStorage](images/AzureDiskStorage.png)

Q: Azure Functions vs Azure Logic Apps?  
A:

![AzureLogicApps](images/AzureLogicApps.png)
# ==================================================
# 03_Manage Subscriptions and Governance
![AzureTypeOfSubscription](images/AzureTypeOfSubscription.png)

![AzureSubscriptionObjects](images/AzureSubscriptionObjects.png)
# ==================================================
# 04_Manage Storage Accounts
![AzureStorageDataObject](images/AzureStorageDataObject.png)

![AzureTypeOfStorageAccount](images/AzureTypeOfStorageAccount.png)

![AzureStorageAccessTier](images/AzureStorageAccessTier.png)

![AzureStorageReplicationOptions](images/AzureStorageReplicationOptions.png)

![AzureStorageAuthorizationTypes](images/AzureStorageAuthorizationTypes.png)
# ==================================================
# 07_Create and Configure Virtual Machines
![AzureVMListOfResourceUsed](images/AzureVMListOfResourceUsed.png)
# ==================================================
# 02h06m_LYNDA_Azure for Architects Design a Networking Strategy 2019
Q: Load balancer vs Application Gateway?  
A:

![LoadBalancerVsApplicationGateway01](images/LoadBalancerVsApplicationGateway01.png)

![LoadBalancerVsApplicationGateway02](images/LoadBalancerVsApplicationGateway02.png)

![ApplicationGatewayKnowsURL](images/ApplicationGatewayKnowsURL.png)

Q: What is the relationship between VNet and Subnet?  
A:

![VNetAndSubnet](images/VNetAndSubnet.png)

![SubnetExample01](images/SubnetExample01.png)

![SubnetExample02](images/SubnetExample02.png)

Also:

      Yes. Azure reserves 5 IP addresses within each subnet. These are x.x.x.0-x.x.x.3 and the last address of the subnet. x.x.x.1-x.x.x.3 is reserved in each subnet for Azure services.
      x.x.x.0: Network address
      x.x.x.1: Reserved by Azure for the default gateway
      x.x.x.2, x.x.x.3: Reserved by Azure to map the Azure DNS IPs to the VNet space
      x.x.x.255: Network broadcast address

source: https://docs.microsoft.com/en-us/azure/virtual-network/virtual-networks-faq#are-there-any-restrictions-on-using-ip-addresses-within-these-subnets

Q: Load balancer vs Traffic Manager?  
A: Traffic Manager is a load balancer for a region.

![TrafficManager](images/TrafficManager.png)

Q: Traffic Manager vs Front Door?  
A: Front door is a global load balancer.

![AzureFrontDoor](images/AzureFrontDoor.png)
# ==================================================
# 02h18m_LYNDA_Azure Administration Configure and Manage Virtual Networking 2020
![VNetOverview](images/VNetOverview.png)

Q: What is the main difference between NSG and Route Table?  
A:

![NetworkSecurityGroupVsRouteTable](images/NetworkSecurityGroupVsRouteTable.png)

Q: What are the 2 types of VNet peering?  
A:
1. Region peering
2. Global peering

![VNetPeeringTypes](images/VNetPeeringTypes.png)
# ==================================================
# 01h03m_PLURALSIGHT_Managing Network Load Balancing in Microsoft Azure by Tim Warner 2019
Q: Types of load balancer?  
A:

![AzureLoadBalancerFamily](images/AzureLoadBalancerFamily.png)

Q: Public vs Internal Load Balancer?  
A:

![InternalLoadBalancer](images/InternalLoadBalancer.png)
# ==================================================
# 01h49m_LYNDA_Azure Administration Manage Subscriptions and Resources 2019
Q: Why use tagging?  
A:

      You can use tags to group your billing data.
      For example, if you're running multiple VMs for different organizations, use the tags to group usage by cost center.
      You can also use tags to categorize costs by runtime environment, such as the billing usage for VMs running in the production environment.

source: https://docs.microsoft.com/en-us/azure/azure-resource-manager/management/tag-resources#tags-and-billing

Q: How many types of RBAC?  
A:

![RBACTypes](images/RBACTypes.png)

![RBACTypeOwner](images/RBACTypeOwner.png)

![RBACTypeContributor](images/RBACTypeContributor.png)

![RBACTypeReader](images/RBACTypeReader.png)

Q: How many types of log?  
A:

![ResourceLog01](images/ResourceLog01.png)

![ResourceLog02](images/ResourceLog02.png)

Q: RBAC vs Policy?  
A:

![RBACVsPolicy](images/RBACVsPolicy.png)

source: https://techcommunity.microsoft.com/t5/itops-talk-blog/governance-101-the-difference-between-rbac-and-policies/ba-p/1015556

Q: What is RBAC scope?  
A:

![RBACScope](images/RBACScope.png)
# ==================================================
# EXTRAS
Q: What is the difference between "Cloud Service Model" vs "Cloud Architect Model"?  
A:
* Cloud Service Model:
   1. Iaas
   2. Paas
   3. Saas
* Cloud Architect Model:
   1. Public
   2. Private
   3. Hybride

Q: Azure VM "Scale Set" vs "Availability Set"?  
A:

See: https://medium.com/awesome-azure/difference-between-scale-set-and-availability-set-in-azure-9b2da03b891c

OR

![AvailabilitySetVsScaleSet](images/AvailabilitySetVsScaleSet.png)

![VMScaleSet](images/VMScaleSet.png)

Q: What are the different types of Azure App Services?
A:
1. Web App
2. Web App for container
3. API App <--- As of 2021, I think it is now renamed to "API Management Service"

Q: What are Azure serverless services offering?
A:
1. Azure Functions
2. Azure Logic App
3. Azure Event Grid

Q: What are Azure DevOps services/products?  
A:

![AzureDevOps](images/AzureDevOps.png)

Q: What are the minimum RBAC roles?  
A:
1. Owner
2. Contributor
3. Reader

Q: What are RBAC Scope?  
A:

![RBACScope](images/RBACScope.png)

![RBACScope02](images/RBACScope02.png)

# ==============================
# Docker on Azure
## Overview
* Azure Kubernetes Service (AKS)
* Azure Container Instances
* Azure Container Registry
## How to create a container registry
```shell
# 1. Create a ResourceGroup
az groupe create --name my-docker-resource-group --location eastus

# 2. Create an ACR instance
az acr create --resource-group my-docker-resource-group --name aks-course --sku basic

# 3. Login to our ACR instance
az acr login --name aks-course

# 4. Get ACR login server address
az acr list --resource-group aks--course --query "[].{acrLoginServer:LoginServer}" --output tsv

export aLS=[YourACRLoginServerName]

# 5. Tag your image with the ACR login Server
sudo docker tag [YourImageName]:[YourImageVersion]:${aLS}/[YourImageName]:[YourImageVersion]

# 6. Verify
sudo docker images

# 7. Push image to ACR
sudo docker push ${aLS}/[YourImageName]:[YourImageVersion]

# 8. Verify your image
az acr repository list --name aks-course --output tsv

# 9. Pull your ACR image
sudo docker pull aks-course.azureacr.io/[YourImageName]:[YourImageVersion]
```